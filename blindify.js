let startButton = document.getElementById('startButton')
let statut = false
let audio = {
    on: new Audio('audio/blindify_on.mp3'),
    off: new Audio('audio/blindify_off.mp3')
}
let textColor = document.getElementById("textOption-color")
let textBackground = document.getElementById("textOption-background")
let textBorder = document.getElementById("textOption-border")
let textRender = document.getElementById("text-option-render")

// If a color already exists locally, update the options
if (localStorage['textRenderColor'] || localStorage['textRenderBackground'] || localStorage['textRenderBorder']) {
    textRender.style.color = localStorage['textRenderColor']
    textColor.value = localStorage['textRenderColor']

    textRender.style.backgroundColor = localStorage['textRenderBackground']
    textBackground.value = localStorage['textRenderBackground']

    textRender.style.border = '5px solid ' + localStorage['textRenderBorder']
    textBorder.value = localStorage['textRenderBorder']
} else {
    setTextRender()
}

let lang = document.get;

startButton.addEventListener("click", () => {
    if (statut === false) {
        startButton.classList.add('active')
        statut = true
        audio.on.play()
        testText()
    } else {
        statut = false
        startButton.classList.remove('active')
        audio.off.play()
    }
})

// Change the text rendering if changing a color
textColor.addEventListener("change", () => setTextRender())
textBackground.addEventListener("change", () => setTextRender())
textBorder.addEventListener("change", () => setTextRender())

function setTextRender() {
    textRender.style.color = textColor.value
    textRender.style.backgroundColor = textBackground.value
    textRender.style.border = '5px solid ' + textBorder.value
    localStorage['textRenderColor'] = textColor.value
    localStorage['textRenderBackground'] = textBackground.value
    localStorage['textRenderBorder'] = textBorder.value
}

function testText() {
    var body = document.body
    var all = body.getElementsByTagName("*")
    Array.prototype.forEach.call(all, element => {
        var elementText = getText(element)
        console.log(elementText)
        if (elementText !== "") {
            textRender.style.color = localStorage['textRenderColor']
            element.style.backgroundColor = localStorage['textRenderBackground']
            element.style.border = '5px solid ' + localStorage['textRenderBorder']
        }
        console.log('--------------------')
    })
    function getText(element) {
        var text = '';
        for (var i = 0; i < element.childNodes.length; ++i)
          if (element.childNodes[i].nodeType === Node.TEXT_NODE)
            text += element.childNodes[i].textContent;        
    }
}

document.addEventListener("DOMContentLoaded", function(event) { 
    document.addEventListener("click",function(e){
        var x = e.clientX, y = e.clientY,
        elementMouseIsOver = document.elementFromPoint(x, y);
        text = elementMouseIsOver.textContent;
        alert(text);
        textspeech(text);
        readaudio();
    }); 
})

function textspeech(){
    // Imports the Google Cloud client library
    const textToSpeech = require('@google-cloud/text-to-speech');
    // Creates a client
    const apikey = { projectId : 'lpweb-266107',  keyFilename : './key.json'};
    // Import other required libraries
    const fs = require('fs');
    const util = require('util');
    // Creates a client
    const client = new textToSpeech.TextToSpeechClient(apikey);
    async function quickStart() {

    // Construct the request
    const request = {
        input: {text: text},
        // Select the language and SSML voice gender (optional)
        voice: {languageCode: 'en-US', ssmlGender: 'NEUTRAL'},
        // select the type of audio encoding
        audioConfig: {audioEncoding: 'MP3'},
    };

    // Performs the text-to-speech request
    const [response] = await client.synthesizeSpeech(request);
    // Write the binary audio content to a local file
    const writeFile = util.promisify(fs.writeFile);
    await writeFile('output.mp3', response.audioContent, 'binary');
    console.log('Audio content written to file: output.mp3');
    }
    quickStart();
}

function readaudio(){
    new Audio('output.mp3').play();
}
