# Blindify
Extension de navigateur pour permettre au malvoyants d'écouter le contenu d'une page.

Pour utiliser l'extension :

- Clonez le projet sur votre poste perso




# Pour ajouter l'extention sur Firefox :

1. Options > Modules complémentaires

2. Cliquez sur la roue crantée ppuis sur "Déboguer des modules"

3. Charger un module complémentaire temporaire ...

4. Sélectionner dans le répo le fichier "blindify.js"

5. L'extention est maintenant sur votre navigateur




# Pour ajouter l'extention sur Chrome :

1. Options > Plus d'outils > Extentions

2. Charger l'extention non empaquetée (en haut à gauche de la fenêtre)

3. Sélectionner le dossier Blindify

4. L'extention est desormais sur votre navigateur



L'application devra permettre de lire le contenu d'une page html à voit haute pour aider les personnes malvoyantes à lire le contenu d'une page.


## Taches réparties

Matisse Aubry : Lecture du code source de la page et modification de l'apparence du texte pour aider les personnes malvoyantes à lire le contenu des pages web

Fabien Lebreuilly : Récupération du code source de la page et conversion de ce texte à l'oral grâce au text-to-speech

Paul Bernasconi : Mise en place du manifeste et traduction du texte en fonction de la langue de l'utilisateur

## Idée générale du projet

Ce projet nous a beaucoup appris lors de sa réalisation, nous n'avons pas réussi à aller au bout de ce projet a cause de quelques points bloquants qui nous ont fortement ralenti.
Se projet sera repris et continué par la suite, l'idée du projet est interessante et révèle un grand challenge technique à relever. Il semble également une bonne idée de passer ce projet en open-source afin de permettre aux développeurs interessés de pouvoir contribuer à ce projet afin d'y ajouter leurs idées d'outils/de fonctionnalitées.
L'idée principale de tout ce projet est avant toput de permettre l'accessibilité aux personnes malvoyantes ainsi qu'aux personnes ne savant pas lire ou/et écrire de pouvoir utiliser internet comme nous le faisons dans la vie de tout les jours.